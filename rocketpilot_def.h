#ifndef _ROCKETPILOT_DEF_H_
#define _ROCKETPILOT_DEF_H_

#define BMP_SCK 13
#define BMP_MISO 12
#define BMP_MOSI 11
#define BMP_CS 9

#define SEALEVELPRESSURE_HPA (1013.25)

#define FILTER 40
#define COEFF_K ((float)0.2)

// configuring pin for maximum altitude management
#define PIN_MAX_DISPLAY_VCC 20
#define PIN_MAX_DISPLAY_IN  21

#define PIN_MAX_RESET_VCC   15
#define PIN_MAX_RESET_IN    16

#define ADDR_ALT_MAX        10


typedef enum 
{
  te_Init, 
  te_PreLaunch,
  te_Liftoff,
  te_Flight,
  te_Descent,
  te_Landed,
} te_statemachine;

#endif
