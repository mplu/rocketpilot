#include <Adafruit_BMP3XX.h>
#include <bmp3.h>
#include <bmp3_defs.h>
#include <Servo.h>
#include <SPI.h> // Include the Arduino SPI library
#include "rocketpilot_def.h"

// Init for serial display (debug purpose)
void InitSerial()
{
  Serial.begin(115200);
  while (!Serial);
}

// Init for motor
void InitMotor()
{
  myservo.attach(pin_servo);  // attache le servo au pin spécifié sur l'objet myservo
  myservo.write(angle_initial);
}

// Init for SPI display
void InitSPI_Display()
{
  // -------- SPI initialization
  pinMode(ssPin, OUTPUT);  // Set the SS pin as an output
  digitalWrite(ssPin, HIGH);  // Set the SS pin HIGH
  SPI.begin();  // Begin SPI hardware
  SPI.setClockDivider(SPI_CLOCK_DIV64);  // Slow down SPI clock
  // --------

  // Clear the display, and then turn on all segments and decimals
  clearDisplaySPI();  // Clears display, resets cursor

  // Flash brightness values at the beginning
  setBrightnessSPI(255);  // High brightness
}

// Init for BMP388
void InitBMP388()
{
  if (!bmp.begin_I2C()) {   // hardware I2C mode, can pass in address & alt Wire
    Serial.println("Could not find a valid BMP3 sensor, check wiring!");
    while (1);
  }
  // Set up oversampling and filter initialization
  bmp.setTemperatureOversampling(BMP3_OVERSAMPLING_8X);
  bmp.setPressureOversampling(BMP3_OVERSAMPLING_4X);
  bmp.setIIRFilterCoeff(BMP3_IIR_FILTER_COEFF_3);
  bmp.setOutputDataRate(BMP3_ODR_50_HZ);
}

// This custom function works somewhat like a serial.print. You can send it an array of chars (string) and it'll print the first 4 characters in the array.
void s7sSendStringSPI(String toSend)
{
  digitalWrite(ssPin, LOW);
  for (int i=0; i<4; i++)
  {
    SPI.transfer(toSend[i]);
  }
  digitalWrite(ssPin, HIGH);
}

// Send the clear display command (0x76) This will clear the display and reset the cursor
void clearDisplaySPI()
{
  digitalWrite(ssPin, LOW);
  SPI.transfer(0x76);  // Clear display command
  digitalWrite(ssPin, HIGH);
}

// Set the displays brightness. Should receive byte with the value to set the brightness to 
//    dimmest------------->brightest
//    0--------127--------255
void setBrightnessSPI(byte value)
{
  digitalWrite(ssPin, LOW);
  SPI.transfer(0x7A);  // Set brightness command byte
  SPI.transfer(value);  // brightness data byte
  digitalWrite(ssPin, HIGH);
}

// Turn on any, none, or all of the decimals.
//  The six lowest bits in the decimals parameter sets a decimal 
//  (or colon, or apostrophe) on or off. A 1 indicates on, 0 off.
//  [MSB] (X)(X)(Apos)(Colon)(Digit 4)(Digit 3)(Digit2)(Digit1)
void setDecimalsSPI(byte decimals)
{
  digitalWrite(ssPin, LOW);
  SPI.transfer(0x77);
  SPI.transfer(decimals);
  digitalWrite(ssPin, HIGH);
}

// display long string using a rolling display
void s7sSendRollinStringSPI(String toSend)
{
  int index = 0,stop_=0;

  while(!stop_)
  {
    digitalWrite(ssPin, LOW);
    for (int i=0+index; i<4+index; i++)
    {
      if(toSend[i]!=0)
      {
        Serial.print(toSend[i]);
        SPI.transfer(toSend[i]);
      }else
      {
        stop_ = 1;
      }
    }
    digitalWrite(ssPin, HIGH);
    Serial.println("");
    index ++;
    delay(1500);
  }
}

void printstate(te_statemachine toprint)
{
  char localstring[50];
  
  switch (toprint)
  {
    case te_Init: 
      sprintf(localstring,"%s","Init");
      break;
    case te_PreLaunch:
      sprintf(localstring,"%s","Prelaunch");
      break;
    case te_Liftoff:
      sprintf(localstring,"%s","Lift off !");
      break;
    case te_Flight:
      sprintf(localstring,"%s","Flight");
      break;
    case te_Descent:
      sprintf(localstring,"%s","Descent");
      break;
    case te_Landed:
      sprintf(localstring,"%s","Landed");
      break;
    default:
      sprintf(localstring,"%s","Unknown");
      break;
  }
  Serial.print(localstring);
}

void InitGPIO()
{
  pinMode(PIN_MAX_DISPLAY_VCC,OUTPUT);
  digitalWrite(PIN_MAX_DISPLAY_VCC,LOW);
  pinMode(PIN_MAX_DISPLAY_IN,INPUT_PULLUP);
  
  pinMode(PIN_MAX_RESET_VCC,OUTPUT);
  digitalWrite(PIN_MAX_RESET_VCC,LOW);
  pinMode(PIN_MAX_RESET_IN,INPUT_PULLUP);

}
