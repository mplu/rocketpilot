#include <Adafruit_BMP3XX.h>
#include <bmp3.h>
#include <bmp3_defs.h>
#include <Servo.h>
#include <SPI.h> // Include the Arduino SPI library
#include <EEPROM.h>
#include "rocketpilot_def.h"

Adafruit_BMP3XX bmp; // creating barometer object
Servo myservo;  // creating barometer object

// configuring variables and pin for servomotor
int pin_servo = 7;       // Pin 7 sur lequel est branché le servo sur l'Arduino si vous utilisez un ESP32 remplacez le 6 par 4 et si vous utilisez un ESP8266 remplacez le 6 par 2
int pos = 0;             // variable permettant de conserver la position du servo
int angle_initial = 0;   //angle initial
int angle_final = 180;   //angle final
int increment = 1;       //incrément entre chaque position
bool angle_actuel = false;//Envoi sur le port série la position courante du servomoteur

// configuring variable and pin for 7seg display
const int ssPin = 8; // Define the SS pin. This is the only pin we can move around to any available digital pin.



// process global variables
unsigned int counter = 0;  // This variable will count up to 65k
char tempString[50];  // Will be used with sprintf to create strings
float altitude_zero = 0, altitude_max = 0;
te_statemachine state = te_Init;

void setup()
{
	InitSerial();
	Serial.println("Rocket Pilot uing BMP388, servo and 7 seg display");

	
  delay(50);
	InitSPI_Display();
  delay(50);
	InitMotor();
  delay(50);
  InitGPIO();
  delay(50);
  InitBMP388();
	
	// Custom function to send four bytes via SPI. The SPI.transfer function only allows sending of a single byte at a time.
	s7sSendStringSPI("-HI-");
	//setDecimalsSPI(0b111111);  // Turn on all decimals, colon, apos
	//s7sSendRollinStringSPI("ROCKET ALTIMETER");
	delay(1500);
	// Clear the display before jumping into loop
	clearDisplaySPI();  
	

}

void loop()
{
	float altitude_raw = 0 ;
	float altitude = 0,delta_altitude,altitude_filtree;
	static float altitude_previous = 0, altitude_filtree_prec = 0;
	static short int counter_descent;
  int pin16 = 0;
  int pin21 = 0;


	altitude_raw = bmp.readAltitude(SEALEVELPRESSURE_HPA);
	
	altitude = altitude_raw - altitude_zero ;

	// filtrage de la valeur
	altitude_filtree = (1 - COEFF_K) * altitude_filtree_prec + COEFF_K * altitude;
	altitude_filtree_prec = altitude_filtree;

	// utilisation de la valeur filtree pour le reste du programme
	altitude = altitude_filtree;

	// detection de max
	if(altitude_max < altitude)
	{
		altitude_max = altitude ;
	}
	delta_altitude = altitude - altitude_previous;
	
	switch (state)
	{
		case te_Init:
			myservo.write(angle_initial);
			if (counter > FILTER)
			{
				state = te_PreLaunch;
				altitude_zero  = altitude_raw;
				//altitude_max = 0;
       EEPROM.get(ADDR_ALT_MAX, altitude_max) ;
			}
			break;
		case te_PreLaunch:
			// altitude is supposed to be around 0
			// considering take off if value is more than 50% higher
			if(altitude > 1)
			{
				state = te_Liftoff;
			}
			// detached motor to save battery, motor couple is high enough to be considered close even without setpoint
			myservo.detach();
			break;
		case te_Liftoff:
			state = te_Flight;
			break;
		case te_Flight:
			// considering descent phase start if value is 0.5 m below max
			if(altitude < altitude_max - 0.5 )
			{
				state = te_Descent;
				myservo.attach(pin_servo);
				counter_descent = 0;
				myservo.write(angle_final); // opening parachute
        EEPROM.put(ADDR_ALT_MAX, altitude_max);
			}
			break;
		case te_Descent:
			// considering landed if value is higher than 95% of previous altitude
			if((delta_altitude < 0.01) && (delta_altitude > -0.01))
			{
				counter_descent ++;
			}
			if (counter_descent > 3)
			{
				state = te_Landed;
				myservo.detach();
			}
			break;
		case te_Landed:
			//while(1);
			break;
		default:
			break;
	}
	
	counter ++;

	// sauvegarde de la valeur precedente
	altitude_previous = altitude;

  if(!digitalRead(PIN_MAX_RESET_IN))
  {
    altitude_max = 0 ;
    EEPROM.put(ADDR_ALT_MAX, altitude_max);
  }else
  {
    pin16 = 0;
  }
  if(!digitalRead(PIN_MAX_DISPLAY_IN))
  {
    pin21 = 1;
  }else
  {
    pin21 = 0;
  }
	
	printstate(state);
	Serial.print("/ Altitude = ");
	Serial.print(altitude);
	Serial.print("/ Altitfiltr = ");
	Serial.print(altitude_filtree);
	Serial.print(" m / Max = ");
	Serial.print(altitude_max);
	Serial.print(" m / delta = ");
	Serial.print(delta_altitude);
	Serial.print(" m / cpt : ");
	Serial.print(counter);
 	Serial.print(" pin 16 (reset) : ");
	Serial.print(pin16);
	Serial.print(" pin 21 (dispmax) : ");
	Serial.print(pin21);
	Serial.println("");

	// display alternatively current altitude and max altitude (using a apostrophe to differentiate
	if(((int)(counter/10)%2 == 1) || (pin21 == 1))
	{
		sprintf(tempString, "%4d", (int)(altitude_max*10));
		setDecimalsSPI(0b00100100);
	}else
	{
		sprintf(tempString, "%4d", (int)(altitude*10));
		setDecimalsSPI(0b00000100);
	}
 

	
	// This will output the tempString to the S7S
	s7sSendStringSPI(tempString);

	delay(50);  // This will make the display update at xxxHz.*/
}
